from typing import List, Dict
from typing_extensions import Annotated
from pathlib import Path
import sys
import yaml
import typer

http_method_names: List[str] = ["get", "post", "put", "delete"]
x_grouping_attribute_name = "x-operation-group"
unspecified_group: str = "unspecified"

app = typer.Typer()

def split_endpoint(endpoint_spec: object) -> Dict[str, object]:
    """
    Splits each endpoint into several endpoints, with each endpoint 
    containing only the operations having the same group.
    """
    grouped_endpoints : Dict[str, object] = {}
    default_group = endpoint_spec.get(x_grouping_attribute_name, unspecified_group)
    for http_method_name in http_method_names:
        operation = endpoint_spec.get(http_method_name)
        if operation:
            operation_group: str = operation.get(x_grouping_attribute_name, default_group)
            if not operation_group in grouped_endpoints:
                new_endpoint_spec = {k: v for k, v in endpoint_spec.items() if k not in http_method_names}
                grouped_endpoints[operation_group] = new_endpoint_spec
            grouped_endpoints[operation_group][http_method_name] = operation
    
    return grouped_endpoints


def make_common_spec_parts(spec: Dict[str, object]) -> Dict[str, object]:
    common_part: Dict[str, object] = {}
    """
    Make sure the split YAMLs come out with the right order of elements.
    """
    for k, v in spec.items():
        if k != "paths":
            if k == "components":
                common_part["paths"] = {}
            common_part[k] = v
    return common_part


def make_report_for_group(group: str, spec: object):
    endpoint_paths: List[str] = []
    for path, endpoint_spec in spec["paths"].items():
        for http_method_name in http_method_names:
            method = endpoint_spec.get(http_method_name)
            if method:
                endpoint_paths.append(http_method_name.upper() + " " + path)

    return {
        "operation_group": group,
        "endpoint_count": len(endpoint_paths),
        "endpoints": endpoint_paths,
    }


def make_report(source_spec_name: str, splitted_specs: Dict[str, object]):
    return {
        "source": source_spec_name,
        "group_by": x_grouping_attribute_name,
        "operation_groups": [
            make_report_for_group(group, spec)
            for group, spec in splitted_specs.items()
        ],
    }


def dump_specs(source_spec_name: str, specs: Dict[str, object]):
    for group, group_spec in specs.items():
            with open(f"{source_spec_name}-{group}.yaml", "w") as file:
                yaml.safe_dump(group_spec, file, sort_keys=False)


def split_spec(spec_file_name: str) -> Dict[str, object]:
    splitted_specs: Dict[str, object] = {}
    with open(spec_file_name, "r") as spec_file:
        spec = yaml.safe_load(spec_file)
        for endpoint_name, endpoint in spec["paths"].items():
            grouped_endpoints = split_endpoint(endpoint)
            for group, grouped_endpoint in grouped_endpoints.items():
                if group not in splitted_specs:
                    splitted_specs[group] = make_common_spec_parts(spec)
                splitted_specs[group]["paths"][endpoint_name] = grouped_endpoint

    return splitted_specs

@app.command(hidden = True)
def placeholder():
    """
    Hidden method to deliver a consistent experience for the callers when we later add other commands.
    """
    pass

@app.command()
def group_endpoints(
        spec: Annotated[Path, typer.Option(help="The OpenAPI specification to analyze and group")],
        preview: Annotated[bool, typer.Option(help="Previews the grouping; results are not written to disk.")] = False
    ):
    """
    Groups endpoint operations into individual specs using the `x-operation-group` classifier.
    """

    base_name = spec.stem
    specs = split_spec(spec)

    if not preview:
        dump_specs(base_name, specs)

    report = make_report(base_name, specs)
    yaml.safe_dump(report, sort_keys=False, stream=sys.stdout)

if __name__ == "__main__":
    app()